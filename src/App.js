import TodoList from './TodoList/TodoList'

// FUNCTIONAL COMPONENT!
function App() {

	return (
		<div>
			<h1>Hello World</h1>
			<TodoList />
		</div>
	);
}

export default App;
