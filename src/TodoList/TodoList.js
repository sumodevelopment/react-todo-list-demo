import { useState, useEffect } from 'react'
import { fetchTodos } from './TodoListAPI'
import TodoListItem from './TodoListItem'

const TodoList = () => {
	// Local state - Hooks
	// 1. - State (VALUE)
	// 2. - Setter (UPDATE STATE) - Function
	const [state, setState] = useState({
		todos: [],
		user: null
	})

	// Hook!
	useEffect(() => { // Lifecylce - ComponentDidMount

		fetchTodos()
			.then(todos => {
				setState({
					...state,
					todos
				})
			})
			.catch(error => {
				console.error(error.message)
			})

			return () => { // Lifecycle - ComponentWillUnmount
				// For Clean up code.
			}

	}, []) // - Lifecyle - ComponentDidUpdate


	// ALWAYS!!! return valid JSX
	return (
		<main>
			<h4>Todos!</h4>
			{ state.todos.map(todo => <TodoListItem key={todo.id} todo={ todo } />) }
			{ state.user && <h4>{ state.user.username }</h4> } 
		</main>
	)
}
export default TodoList