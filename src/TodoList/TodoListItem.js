const TodoListItem = props => {
	return (
		<li>
			<h4>{ props.todo.title }</h4>
			<p>{ props.todo.completed }</p>
		</li>
	)
}
export default TodoListItem